cl-split-sequence (1:2.0.1-2) UNRELEASED; urgency=medium

  * Remove myself from Uploaders

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 10 Jun 2023 13:55:03 +0200

cl-split-sequence (1:2.0.1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Sébastien Villemot ]
  * New upstream release
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 02 Nov 2021 17:02:47 +0100

cl-split-sequence (1:2.0.0-1) unstable; urgency=medium

  * New upstream release
  * Bump to debhelper compat level 12
  * Bump S-V to 4.4.1

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 18 Jan 2020 21:49:15 +0100

cl-split-sequence (1:1.5.0-1) unstable; urgency=medium

  * New upstream release
  * d/copyright: reflect upstream changes (now MIT-licensed)
  * Display more explicit messages for autopkgtest failures
  * Add Rules-Requires-Root: no
  * Bump S-V to 4.2.1

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 07 Oct 2018 17:46:55 +0200

cl-split-sequence (1:1.4.1-1) unstable; urgency=medium

  * New upstream release.
    Epoch added because we're now following numbered releases, as Quicklisp
    does.
  * Drop debian.patch, no longer needed.
  * Mark as M-A foreign.
  * Bump to debhelper compat level 11.
  * Rewrite d/rules using dh.
  * Rewrite d/copyright in machine-readable format 1.0.
  * Bump S-V to 4.1.4.
  * Add myself to Uploaders.
  * Add Recommends on cl-fiveam for the testsuite.
  * Add autopkgtest that runs the testsuite on sbcl, ecl and clisp.

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 28 Apr 2018 11:14:30 +0200

cl-split-sequence (20050802-4) unstable; urgency=low

  * Team upload.
  * Update Vcs-* fields for move to salsa.
  * Remove ${shlibs:Depends} (it's an arch:all package).
  * Set Maintainer to debian-common-lisp@l.d.o.
  * Move to 3.0 (quilt) source format.
    Changes to upstream source now under d/p/debian.patch.
  * Remove Build-Depends on dh-lisp.
  * Remove obsolete README.building and README.Debian.
  * Fix typo in short package description. (Closes: #697988)
  * Use secure URL for Homepage.
  * Create d/watch (tracking versioned releases).

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 10 Apr 2018 10:49:27 +0200

cl-split-sequence (20050802-3) unstable; urgency=low

  * changed to lisp section
  * Updated Standards-Version no real changes
  * Now use debhelper v7
  * Now use dh_lisp

 -- Peter Van Eynde <pvaneynd@debian.org>  Thu, 03 Sep 2009 16:35:33 +0100

cl-split-sequence (20050802-2) unstable; urgency=low

  * Changed to group maintanance
  * Added Vcs-Git control field
  * debhelper is Build-Depends
  * swap binary-indep and binary-arch
  * Now uses debian/compat
  * Added homepage field
  * Updated standard version without real changes

 -- Peter Van Eynde <pvaneynd@debian.org>  Sat, 23 Feb 2008 15:49:11 +0100

cl-split-sequence (20050802-1) unstable; urgency=low

  * Now uses darcs
  * Updated standard version
  * Recovered original upstream sources, no real changes.

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue,  2 Aug 2005 05:30:55 +0200

cl-split-sequence (20011114.1-2) unstable; urgency=low

  * New maintainer. (Closes: #297409: O: cl-split-sequence -- Common
    Lisp package split a sequence of objects)
  * Adopted by Peter Van Eynde
  * Updated standard version.

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue,  1 Mar 2005 23:05:14 +0100

cl-split-sequence (20011114.1-1) unstable; urgency=low

  * Initial release

 -- Kevin M. Rosenberg <kmr@debian.org>  Thu, 31 Oct 2002 07:07:34 -0700
